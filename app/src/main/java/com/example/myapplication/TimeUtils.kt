package com.example.myapplication

import android.content.Context

object TimeUtils {
    fun convertMinutesToHours(min: Int) = min / 60

    fun calculateMin(min: Int) = min % 60

    fun convertHoursWithMinutesToMinutes(hours: Int, min: Int) = hours * 60 + min

    fun calculateDifferenceTimes(context: Context, minFirst: Int, minSec: Int): String {
        if (minFirst <= minSec) {
            val diffMin = minSec - minFirst
            val resultMin = calculateMin(diffMin)
            val resultHours = convertMinutesToHours(diffMin)
            return String.format(context.getString(R.string.time_undertime, resultHours, resultMin))
        } else {
            val diffMin = minFirst - minSec
            val resultMin = calculateMin(diffMin)
            val resultHours = convertMinutesToHours(diffMin)
            return String.format(context.getString(R.string.time_overtime, resultHours, resultMin))
        }
    }
}