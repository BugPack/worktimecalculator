package com.example.myapplication

import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.text.method.DigitsKeyListener
import android.util.DisplayMetrics
import android.util.Log
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.LinearLayout.HORIZONTAL
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import androidx.core.widget.doAfterTextChanged
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val digitsKeyListener = DigitsKeyListener.getInstance("0123456789")
    private val inputType = (InputType.TYPE_NUMBER_FLAG_DECIMAL
            or InputType.TYPE_CLASS_NUMBER
            or InputType.TYPE_NUMBER_FLAG_SIGNED)
    private var resultHours = 0
    private var summMin = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        generateDayOfWeek()
        etCost?.doAfterTextChanged {
            calculateCost()
        }
    }

    private fun generateDayOfWeek() {
        for (i in 0 until 7) {
            val label = TextView(this)
            label.text = getString(DayOfWeek.values()[i].nameDayResId)
            linearLayout?.addView(label)

            val editTextHour = EditText(this)
            editTextHour.inputType = inputType
            editTextHour.keyListener = digitsKeyListener
            editTextHour.width = calculateDisplayWidth() / 2
            editTextHour.hint = getString(R.string.hours)
            editTextHour.addValidationListener(23)

            val editTextMin = EditText(this)
            editTextMin.inputType = inputType
            editTextMin.keyListener = digitsKeyListener
            editTextMin.width = calculateDisplayWidth() / 2
            editTextMin.hint = getString(R.string.minutes)
            editTextMin.addValidationListener(59)

            etHoursInWeek.addValidationListener(168)

            val childLayout = LinearLayout(this)
            childLayout.orientation = HORIZONTAL
            childLayout.apply {
                addView(editTextHour)
                addView(editTextMin)
            }

            linearLayout?.addView(childLayout)
        }
    }

    private fun EditText.addValidationListener(maxSize: Int) {
        this.doAfterTextChanged {
            if (this.text.isNotEmpty() && this.text.toString().toInt() > maxSize) {
                this.setText("$maxSize")
            }
            calculateHoursAndMinutes()

            var hoursInWeek = 0
            if (!etHoursInWeek?.text.isNullOrEmpty()) {
                hoursInWeek = etHoursInWeek?.text.toString().toInt()
            }
            tvSummTime?.text = String.format(
                getString(R.string.time_summ), resultHours, TimeUtils.calculateMin(summMin),
                TimeUtils.calculateDifferenceTimes(
                    context,
                    TimeUtils.convertHoursWithMinutesToMinutes(
                        resultHours, TimeUtils.calculateMin(summMin)
                    ),
                    TimeUtils.convertHoursWithMinutesToMinutes(hoursInWeek, 0)
                )
            )
            calculateCost()
        }
    }

    private fun calculateDisplayWidth(): Int {
        val outMetrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            display?.getRealMetrics(outMetrics)
        } else {
            windowManager?.defaultDisplay?.getRealMetrics(outMetrics)
        }
        return outMetrics.widthPixels
    }

    private fun calculateHoursAndMinutes() {
        resultHours = 0
        summMin = 0
        for (i in 1 until 14 step 2) {
            val etHours = ((linearLayout.getChildAt(i) as ViewGroup)[0] as EditText).text
            if (etHours.isNotEmpty()) {
                resultHours += etHours.toString().toInt()
            }
            val etMinutes = ((linearLayout.getChildAt(i) as ViewGroup)[1] as EditText).text
            if (etMinutes.isNotEmpty()) {
                summMin += etMinutes.toString().toInt()
            }
        }
        resultHours += TimeUtils.convertMinutesToHours(summMin)
        Log.d("_log", "$resultHours")
    }

    private fun calculateCost() {
        var resultCost = 0
        if (etCost.text.isNotEmpty()) {
            resultCost = etCost.text.toString().toInt() * resultHours
            tvCostResult?.text = String.format(getString(R.string.cost_result), resultCost)
        } else {
            tvCostResult?.text = getString(R.string.please_input_cost)
        }
    }
}